<?php

namespace App\Tests\Service;

use App\Service\TimeService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class TimeServiceTest
 * @package App\Tests\Service
 */
class TimeServiceTest extends WebTestCase
{
    const MARTIAN_CALCULATED_SOLAR_TIME = '51981.09749';
    const MARTIAN_CALCULATED_COORDINATED_TIME = '02:20:23';
    const EARTH_CHECK_TIME = '2020-03-23 15:00:00';

    /**
     * @var TimeService
     */
    private $timeService;

    /**
     *
     */
    protected function setUp()
    {
        static::bootKernel();

        $this->timeService = static::$kernel->getContainer()->get('App\Service\TimeService');
    }

    /**
     * @throws \Exception
     */
    public function testGetMartianTimeIsArray()
    {
        self::bootKernel();

        $earthTime = new \DateTime(self::EARTH_CHECK_TIME);

        $martianTime = $this->timeService->getMartianTime($earthTime);

        $this->assertIsArray($martianTime);
    }

    /**
     * @throws \Exception
     */
    public function testGetMartianTimeContainsBothTimes()
    {
        self::bootKernel();

        $earthTime = new \DateTime(self::EARTH_CHECK_TIME);

        $martianTime = $this->timeService->getMartianTime($earthTime);

        $this->assertArrayHasKey('solar_time', $martianTime);

        $this->assertArrayHasKey('coordinated_time', $martianTime);
    }

    /**
     * @throws \Exception
     */
    public function testGetMartianTimeReturnsProperTimes()
    {
        self::bootKernel();

        $earthTime = new \DateTime(self::EARTH_CHECK_TIME);

        $martianTime = $this->timeService->getMartianTime($earthTime);

        $this->assertEquals(self::MARTIAN_CALCULATED_SOLAR_TIME, $martianTime['solar_time']);

        $this->assertEquals(self::MARTIAN_CALCULATED_COORDINATED_TIME, $martianTime['coordinated_time']);
    }
}