<?php

namespace App\Tests\DataProvider;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class TestMartianTimeDataProvider
 * @package App\Tests\DataProvider
 */
class MartianTimeDataProviderTest extends WebTestCase
{
    const MARTIAN_CALCULATED_SOLAR_TIME = '51981.09749';
    const MARTIAN_CALCULATED_COORDINATED_TIME = '02:20:23';
    const EARTH_CHECK_TIME = '2020-03-23 15:00:00';

    /**
     * @throws \Exception
     */
    public function testCalculatedMartianTimeReturnsProperDatetime()
    {
        self::bootKernel();

        $martianDataProvider = self::$container->get('App\DataProvider\MartianTimeDataProvider');

        $earthTime = new \DateTime(self::EARTH_CHECK_TIME);

        $martianTime = $martianDataProvider->calculateDateTime($earthTime);

        $this->assertEquals( self::MARTIAN_CALCULATED_SOLAR_TIME, $martianTime->getSolarDateTime());
        $this->assertEquals( self::MARTIAN_CALCULATED_COORDINATED_TIME, $martianTime->getCoordinatedDateTime());
    }
}