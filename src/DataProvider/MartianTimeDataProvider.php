<?php

namespace App\DataProvider;

/**
 * Class MartianTimeDataProvider
 * @package App\DataProvider
 */
class MartianTimeDataProvider implements TimeDataProviderInterface
{
    private const SECONDS_PER_SOL = 88775.244147;
    private const LEAP_SECONDS = 37;
    private const MSD_PRECISION = 5;
    private const MCT_FORMAT = 'H:i:s';

    /**
     * @var string
     */
    private $solarTime;

    /**
     * @var string
     */
    private $coordinatedTime;

    /**
     * @param \DateTime $earthDateTime
     * @return MartianTimeDataProvider
     */
    public function calculateDateTime(\DateTime $earthDateTime): TimeDataProviderInterface
    {
        return $this
            ->setSolarTime($earthDateTime->getTimestamp())
            ->setCoordinatedTime();
    }

    /**
     * @return string
     */
    public function getSolarDateTime(): string
    {
        return $this->solarTime;
    }

    /**
     * @param int $timestamp
     * @return MartianTimeDataProvider
     */
    private function setSolarTime(int $timestamp): self
    {
        $marsSolDate = ($timestamp + self::LEAP_SECONDS) / self::SECONDS_PER_SOL + 34127.2954262;
        $this->solarTime = round($marsSolDate, self::MSD_PRECISION, PHP_ROUND_HALF_UP);
        return $this;
    }

    /**
     * @return string
     */
    public function getCoordinatedDateTime(): string
    {
        return $this->coordinatedTime;
    }

    /**
     *
     */
    private function setCoordinatedTime(): self
    {
        $martianCoordinatedTime = round(fmod($this->solarTime, 1) * 86400, 0, PHP_ROUND_HALF_UP);
        $this->coordinatedTime = gmdate(self::MCT_FORMAT, (int) $martianCoordinatedTime);
        return $this;
    }
}