<?php

namespace App\DataProvider;

/**
 * Interface TimeDataProviderInterface
 * @package App\DataProvider
 */
interface TimeDataProviderInterface
{
    /**
     * @param \DateTime $earthDateTime
     * @return TimeDataProviderInterface
     */
    public function calculateDateTime(\DateTime $earthDateTime): self ;

    /**
     * @return string
     */
    public function getSolarDateTime(): string ;

    /**
     * @return string
     */
    public function getCoordinatedDateTime(): string ;

}