<?php

namespace App\Service;

use App\DataProvider\TimeDataProviderInterface;
use DateTime;

/**
 * Class TimeService
 * @package App\Service
 */
class TimeService
{
    /**
     * @var TimeDataProviderInterface
     */
    private $timeDataProvider;

    /**
     * TimeService constructor.
     * @param TimeDataProviderInterface $dataProvider
     */
    public function __construct(TimeDataProviderInterface $dataProvider)
    {
        $this->timeDataProvider = $dataProvider;
    }

    /**
     * @param DateTime $earthDateTime
     * @return array
     */
    public function getMartianTime(DateTime $earthDateTime): array
    {
        $martianTime = $this->timeDataProvider->calculateDateTime($earthDateTime);

        return [
            'solar_time' => $martianTime->getSolarDateTime(),
            'coordinated_time' => $martianTime->getCoordinatedDateTime(),
        ];
    }
}