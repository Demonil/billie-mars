<?php

namespace App\Controller;

use DateTime;
use Exception;
use App\Service\TimeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/api/v1")
 */
class DefaultController extends AbstractController
{
    /**
     * @var TimeService
     */
    private $timeService;

    /**
     * DefaultController constructor.
     * @param TimeService $timeService
     */
    public function __construct(TimeService $timeService)
    {
        $this->timeService = $timeService;
    }

    /**
     * @Route("/time/martian", name="get_martian_time", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function martianTimeAction(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        if (!empty($data['earth_time'])) {
            $earthDateTime = new DateTime($data['earth_time']);

            $martianDateTime = $this->timeService->getMartianTime($earthDateTime);

            return new JsonResponse($martianDateTime);
        }

        throw new \InvalidArgumentException('Earth date time was not provided');
    }
}