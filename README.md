# Introduction

We are proud to introduce the new micro-service which built to provide the transformation of the Earth Time
to the Martian one. Time on Earth and Mars ticking differently(the Martian day is pretty close to ours - 24 hours and 37 minutes,
but the year is almost 2 time longer then on Earth - 687 days). Due to the fact that end users want to be synchronized between
the Earth and Martian time we provide the ultimate solution - Earth-Mars DateTime translator.

This service built with **Symfony 5** framework over **PHP7**. We assume that the microservice will run
within the secured environment so authorization/authentication is not covered in the code, but, of course could be refactored any time
upon the request.

# Installation and build

This microservice if fully independent and standalone service which could be used in any environment.

#### Download
Run this installer to create a binary called *symfony*
(in the next version of the service it will be containerized - don't worry)

```
$ curl -sS https://get.symfony.com/cli/installer | bash
```
Clone the repository to your local machine. 

`$ git clone git@bitbucket.org:Demonil/billie-mars.git`

Change the directory to the service

```
$ cd billie-mars
```

Install the dependencies by running(make sure you have the composer installed) 

```
$ composer install
```

#### Run
Change directory to the project root and run the following

```$ symfony server:start```

Voilà - you service is up and running now at http://localhost:8000

#### Execute time transformaiton
In order to get the Martian time(both Solar and Coordinated) send the POST request to
http://localhost:8000/api/v1/time/martian with the following body(*Content-Type* Header is *application/json*):
```
{
	"earth_time": "2020-03-23 15:00:00"
}
```

Expected result has to be as following
```
{
    "solar_time": "51981.09749",
    "coordinated_time": "02:20:23"
}
```

#### Build and test
In order to run the tests(we don't want the people life's saving service will not be tested, aren't we) input the following command

```$ bin/phpunit```